FROM ruby:2.7.1-alpine
COPY . .

# --
# EnvVars
# Ruby
# --

ENV BUNDLE_HOME=/usr/local/bundle
ENV BUNDLE_APP_CONFIG=/usr/local/bundle
ENV BUNDLE_BIN=/usr/local/bundle/bin
ENV GEM_BIN=/usr/gem/bin
ENV GEM_HOME=/usr/gem


# --
# EnvVars
# Image
# --
ENV JEKYLL_VAR_DIR=/var/jekyll
ENV JEKYLL_VERSION=4.0.0
ENV JEKYLL_DATA_DIR=/var/www
ENV JEKYLL_BIN=/usr/jekyll/bin
ENV JEKYLL_ENV=production


# --
# EnvVars
# System
# --
ENV LANG=de_DE.UTF-8
ENV LANGUAGE=de_DE:en
ENV TZ=Europe/Berlin
ENV PATH="$JEKYLL_BIN:$PATH"
ENV PATH="$GEM_BIN:$PATH"
ENV LC_ALL=de_DE.UTF-8
ENV LANG=de_DE.UTF-8
ENV LANGUAGE=de_DE

# --
# EnvVars
# Main
# --
ENV VERBOSE=false
ENV FORCE_POLLING=false
ENV DRAFTS=false

# --
# Packages
# User
# --
RUN apk --no-cache add \
  ruby-dev \
  build-base \
  curl \
  vim \
  wget \
  bash \
  jq

# --
# Packages
# Dev
# --
RUN apk --no-cache add \
  zlib-dev \
  build-base \
  libxml2-dev \
  libxslt-dev \
  readline-dev \
  libffi-dev \
  yaml-dev \
  zlib-dev \
  cmake

# --
# Packages
# Main
# --
RUN apk --no-cache add \
  linux-headers \
  openjdk8-jre \
  less \
  zlib \
  libxml2 \
  readline \
  libxslt \
  libffi \
  git \
  nodejs \
  tzdata \
  shadow \
  su-exec \
  nodejs-npm \
  libressl \
  yarn

# --
# Gems
# Update
# --
RUN echo "gem: --no-ri --no-rdoc" > ~/.gemrc
RUN unset GEM_HOME && unset GEM_BIN && \
  yes | gem update --system

# --
# Gems
# Main
# --
# Work around a nonsense RubyGem permission bug.
RUN unset GEM_HOME && unset GEM_BIN && yes | gem install --force bundler
RUN gem install jekyll -v$JEKYLL_VERSION -- \
  --use-system-libraries

RUN bundle install

# --
ARG RUNNER_GID
ARG RUNNER_UID
RUN addgroup -Sg $RUNNER_GID jekyll
RUN adduser  -Su $RUNNER_UID -G \
  jekyll jekyll

# --
COPY entrypoint.sh $JEKYLL_BIN/entrypoint
RUN chown jekyll:jekyll $JEKYLL_BIN/entrypoint
RUN chmod u+x $JEKYLL_BIN/entrypoint

# --
RUN mkdir -p $JEKYLL_VAR_DIR
RUN mkdir -p $JEKYLL_DATA_DIR
RUN chown -R jekyll:jekyll $JEKYLL_DATA_DIR
RUN chown -R jekyll:jekyll $JEKYLL_VAR_DIR
RUN chown -R jekyll:jekyll $BUNDLE_HOME

# --
RUN rm -rf /root/.gem
RUN rm -rf /home/jekyll/.gem
RUN rm -rf $BUNDLE_HOME/cache
RUN rm -rf $GEM_HOME/cache

# --
CMD ["jekyll", "--help"]
ENTRYPOINT ["/usr/jekyll/bin/entrypoint"]
WORKDIR /var/www
VOLUME  /var/www
